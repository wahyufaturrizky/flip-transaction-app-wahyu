import React from 'react';
import {View} from 'react-native';
import {MarginEnum, PaddingEnum} from 'styles/Spacer';

export const Row = (props) => {
  return (
    <View
      style={{
        flexDirection: props.flexDirection === 'column' ? 'column' : 'row',
        padding: props.padding,
        margin: MarginEnum['0x'],
        marginTop: props.marginTop,
        marginBottom: props.marginBottom,
        flexWrap: props.flexWrap || 'nowrap',
        alignItems: props.alignItems || 'center',
        justifyContent: props.justifyContent,
        marginLeft: props.marginLeft,
        marginVertical: props.marginVertical,
        backgroundColor: props.backgroundColor,
      }}>
      {props.children}
    </View>
  );
};
