import {Button} from 'components/Button';
import {Text} from 'components/Text';
import {BasicLayout} from 'layout/BasicLayout';
import {Col} from 'layout/Col';
import {Row} from 'layout/Row';
import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  ColorBaseEnum,
  ColorBaseGrayEnum,
  ColorPrimaryEnum,
} from 'styles/Colors';
import {
  BorderRadiusEnum,
  FontWeightSizeEnum,
  MarginEnum,
  PaddingEnum,
  SizeEnum,
} from 'styles/Spacer';
import {currencyFormatter} from 'utils/currencyFormatter';

const DetailTransaction = (props) => {
  const state = {
    amount: props.route.params.amount,
    beneficiary_bank: props.route.params.beneficiary_bank,
    beneficiary_name: props.route.params.beneficiary_name,
    created_at: props.route.params.created_at,
    remark: props.route.params.remark,
    sender_bank: props.route.params.sender_bank,
    unique_code: props.route.params.unique_code,
    id: props.route.params.id,
    account_number: props.route.params.account_number,
  };
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  const {
    amount,
    beneficiary_bank,
    beneficiary_name,
    created_at,
    remark,
    sender_bank,
    unique_code,
    id,
    account_number,
  } = state;

  return (
    <BasicLayout>
      <View
        style={{
          backgroundColor: ColorBaseEnum.white,
          paddingHorizontal: PaddingEnum['2x'],
          borderRadius: BorderRadiusEnum['2x'],
          marginTop: MarginEnum['2x'],
        }}>
        <View
          style={{
            borderBottomColor: ColorBaseGrayEnum.gray300,
            borderBottomWidth: 1,
            paddingVertical: PaddingEnum['3x'],
          }}>
          <Row>
            <Col>
              <Text
                color={ColorBaseGrayEnum.gray600}
                fontWeight={FontWeightSizeEnum.bold}
                label={`ID TRANSAKSI: #${id}  `}
              />
            </Col>
            <Col>
              <TouchableOpacity>
                <IconMaterialCommunityIcons
                  name="content-copy"
                  size={SizeEnum['5x']}
                  color={ColorPrimaryEnum.orange}
                />
              </TouchableOpacity>
            </Col>
          </Row>
        </View>

        <View
          style={{
            borderBottomColor: ColorBaseGrayEnum.gray300,
            borderBottomWidth: 1,
            paddingVertical: PaddingEnum['3x'],
          }}>
          <Row justifyContent="space-between">
            <Col>
              <Text
                color={ColorBaseGrayEnum.gray600}
                fontWeight={FontWeightSizeEnum.bold}
                label="DETAIL TRANSAKSI"
              />
            </Col>
            <Col>
              <Button.Link onPress={() => props.navigation.goBack()}>
                <Text label="Tutup" color={ColorPrimaryEnum.orange} />
              </Button.Link>
            </Col>
          </Row>
        </View>

        <View
          style={{
            paddingVertical: PaddingEnum['3x'],
          }}>
          <Row marginBottom={MarginEnum['2x']}>
            <Col>
              <Text
                color={ColorBaseGrayEnum.gray600}
                fontWeight={FontWeightSizeEnum.bold}
                label={
                  sender_bank.length > 3
                    ? sender_bank.charAt(0).toUpperCase() + sender_bank.slice(1)
                    : sender_bank.toUpperCase()
                }
              />
            </Col>
            <Col>
              <IconIonicons
                size={SizeEnum['5x']}
                name="arrow-forward"
                color={ColorBaseGrayEnum.gray600}
              />
            </Col>
            <Col>
              <Text
                color={ColorBaseGrayEnum.gray600}
                fontWeight={FontWeightSizeEnum.bold}
                label={
                  beneficiary_bank.length > 3
                    ? beneficiary_bank.charAt(0).toUpperCase() +
                      beneficiary_bank.slice(1)
                    : beneficiary_bank.toUpperCase()
                }
              />
            </Col>
          </Row>

          <Row marginBottom={MarginEnum['4x']}>
            <Col size={7}>
              <Text
                variant="large"
                color={ColorBaseGrayEnum.gray600}
                label={beneficiary_name.toUpperCase()}
                fontWeight={FontWeightSizeEnum.bold}
              />
              <Text
                variant="small"
                color={ColorBaseGrayEnum.gray600}
                label={account_number}
              />
            </Col>
            <Col size={3}>
              <Text
                variant="large"
                color={ColorBaseGrayEnum.gray600}
                label="NOMINAL"
                fontWeight={FontWeightSizeEnum.bold}
              />
              <Text
                variant="small"
                color={ColorBaseGrayEnum.gray600}
                label={currencyFormatter(amount)}
              />
            </Col>
          </Row>

          <Row marginBottom={MarginEnum['4x']}>
            <Col size={7}>
              <Text
                variant="large"
                color={ColorBaseGrayEnum.gray600}
                label="BERITA TRANSFER"
                fontWeight={FontWeightSizeEnum.bold}
              />
              <Text
                variant="small"
                color={ColorBaseGrayEnum.gray600}
                label={remark}
              />
            </Col>
            <Col size={3}>
              <Text
                variant="large"
                color={ColorBaseGrayEnum.gray600}
                label="KODE UNIK"
                fontWeight={FontWeightSizeEnum.bold}
              />
              <Text
                variant="small"
                color={ColorBaseGrayEnum.gray600}
                label={unique_code}
              />
            </Col>
          </Row>

          <Row marginBottom={MarginEnum['4x']}>
            <Col>
              <Text
                variant="arge"
                color={ColorBaseGrayEnum.gray600}
                label="WAKTU DIBUAT"
                fontWeight={FontWeightSizeEnum.bold}
              />
              <Text
                variant="small"
                color={ColorBaseGrayEnum.gray600}
                label={`${new Date(created_at).getDate()} ${
                  monthNames[new Date(created_at).getMonth()]
                } ${new Date(created_at).getFullYear()}`}
              />
            </Col>
          </Row>
        </View>
      </View>
    </BasicLayout>
  );
};

export default DetailTransaction;
