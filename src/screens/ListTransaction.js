import {Button} from 'components/Button';
import {ModalNormal} from 'components/Modal/ModalNormal';
import {ModalSpinner} from 'components/Modal/ModalSpinner';
import Navbar from 'components/Navbar/Navbar';
import {Text} from 'components/Text';
import {BasicLayout} from 'layout/BasicLayout';
import {Col} from 'layout/Col';
import {Row} from 'layout/Row';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {getListTransaction} from 'services/retrieveData';
import {
  ColorBaseEnum,
  ColorBaseGrayEnum,
  ColorPrimaryEnum,
  ColorSemanticDangerEnum,
  ColorSemanticPositiveEnum,
} from 'styles/Colors';
import {
  BorderRadiusEnum,
  FontWeightSizeEnum,
  MarginEnum,
  PaddingEnum,
  SizeEnum,
} from 'styles/Spacer';
import {currencyFormatter} from 'utils/currencyFormatter';
import Badge from '../components/Badge/Badge';

const ListTransaction = (props) => {
  const [loadingVisible, setLoadingVisible] = useState({
    isShowLoading: false,
    label: '',
  });
  const [dataListTransaction, setDataListTransaction] = useState(null);
  const [isShowModalFilter, setIsShowModalFilter] = useState(false);
  const [dataFilter, setDataFilter] = useState({
    name: 'URUTKAN',
  });
  const [keyword, setKeyword] = useState(null);
  const [listDataSearch, setListDataSearch] = useState([]);
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const valueFilter = [
    {name: 'URUTKAN'},
    {name: 'Nama A-Z'},
    {name: 'Nama Z-A'},
    {name: 'Tanggal Terbaru'},
    {name: 'Tanggal Terlama'},
  ];
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  useEffect(() => {
    const fetchListTransaction = async () => {
      setLoadingVisible({
        isShowLoading: true,
        label: 'Mohon tunggu sebentar, halaman sedang diproses',
      });
      const response = await getListTransaction().catch((error) => {
        setLoadingVisible({...loadingVisible, isShowLoading: false});
      });

      if (response && response.status === 200) {
        setDataListTransaction(response.data);
        setLoadingVisible({...loadingVisible, isShowLoading: false});
      }
    };

    const filterListTransaction = () => {
      const ordered = Object.keys(dataListTransaction)
        .sort()
        .reduce((obj, key) => {
          obj[key] = dataListTransaction[key];

          return obj;
        }, {});
      setDataListTransaction(ordered);
    };

    fetchListTransaction();
    if (
      dataFilter.name === 'Nama A-Z' ||
      dataFilter.name === 'Nama Z-A' ||
      dataFilter.name === 'Tanggal Terbaru' ||
      dataFilter.name === 'Tanggal Terlama'
    ) {
      filterListTransaction();
    }
  }, [dataFilter.name]);

  const handleSearch = (data) => {
    let dataSearch = null;
    dataSearch = Object.keys(dataListTransaction).filter(
      (field) =>
        dataListTransaction[field].beneficiary_name?.toLowerCase() ===
          data?.toLowerCase() ||
        dataListTransaction[field].beneficiary_bank?.toLowerCase() ===
          data?.toLowerCase() ||
        dataListTransaction[field].sender_bank?.toLowerCase() ===
          data?.toLowerCase() ||
        dataListTransaction[field].amount === parseInt(data),
    );

    setListDataSearch(dataSearch);
  };

  return (
    <>
      <View
        style={{
          backgroundColor: ColorBaseEnum.white,
          paddingBottom: PaddingEnum['3x'],
          paddingHorizontal: PaddingEnum['2x'],
        }}>
        <Row>
          <Col size={6}>
            <Navbar.Search
              searchText={keyword}
              setSearchText={(value) => {
                setKeyword(value);
                handleSearch(value);
              }}
              icon="search"
              placeholder="Nama, bank, nominal"
            />
          </Col>
          <Col size={4} alignItems="center">
            <Button.Link onPress={() => setIsShowModalFilter(true)}>
              <Row>
                <Col>
                  <Text
                    label={dataFilter?.name}
                    color={ColorPrimaryEnum.orange}
                  />
                </Col>
                <Col>
                  <IconMaterialIcons
                    name="keyboard-arrow-down"
                    color={ColorPrimaryEnum.orange}
                    size={SizeEnum['5x']}
                  />
                </Col>
              </Row>
            </Button.Link>
          </Col>
        </Row>
      </View>
      <BasicLayout>
        <View style={{paddingBottom: PaddingEnum['2x']}}>
          {listDataSearch && listDataSearch.length > 0
            ? listDataSearch.map((data, index) => {
                const {
                  amount,
                  beneficiary_bank,
                  sender_bank,
                  beneficiary_name,
                  status,
                  created_at,
                } = dataListTransaction[data];
                return (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate(
                        'DetailTransaction',
                        dataListTransaction[data],
                      )
                    }
                    key={index}>
                    <View
                      style={{
                        marginTop: MarginEnum['2x'],
                        paddingLeft: PaddingEnum['2x'],
                        borderTopLeftRadius: BorderRadiusEnum['2x'],
                        borderBottomLeftRadius: BorderRadiusEnum['2x'],
                        backgroundColor:
                          status === 'SUCCESS'
                            ? ColorSemanticPositiveEnum.light
                            : ColorSemanticDangerEnum.light,
                        borderTopRightRadius: BorderRadiusEnum['2x'],
                        borderBottomRightRadius: BorderRadiusEnum['2x'],
                      }}>
                      <View
                        style={{
                          padding: PaddingEnum['2x'],
                          backgroundColor: ColorBaseEnum.white,
                          borderTopRightRadius: BorderRadiusEnum['2x'],
                          borderBottomRightRadius: BorderRadiusEnum['2x'],
                        }}>
                        <Row justifyContent="space-between">
                          <Col>
                            <Row>
                              <Col>
                                <Text
                                  label={
                                    sender_bank.length > 3
                                      ? sender_bank.charAt(0).toUpperCase() +
                                        sender_bank.slice(1)
                                      : sender_bank.toUpperCase()
                                  }
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="large"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <IconIonicons
                                  size={SizeEnum['5x']}
                                  name="arrow-forward"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <Text
                                  label={
                                    beneficiary_bank.length > 3
                                      ? beneficiary_bank
                                          .charAt(0)
                                          .toUpperCase() +
                                        beneficiary_bank.slice(1)
                                      : beneficiary_bank.toUpperCase()
                                  }
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="large"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                            </Row>
                            <Text
                              label={beneficiary_name.toUpperCase()}
                              color={ColorBaseGrayEnum.gray600}
                            />
                            <Row>
                              <Col>
                                <Text
                                  label={currencyFormatter(amount)}
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="small"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <IconEntypo
                                  size={SizeEnum['5x']}
                                  name="dot-single"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <Text
                                  label={`${new Date(created_at).getDate()} ${
                                    monthNames[new Date(created_at).getMonth()]
                                  } ${new Date(created_at).getFullYear()}`}
                                  fontWeight={FontWeightSizeEnum.bold}
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                            </Row>
                          </Col>
                          <Col>
                            <Badge.Normal
                              variant={
                                status === 'SUCCESS' ? 'success' : 'danger'
                              }
                              borderRadius={BorderRadiusEnum['1x']}
                              backgroundColor={
                                status === 'SUCCESS'
                                  ? undefined
                                  : ColorBaseEnum.white
                              }
                              borderColor={
                                status === 'SUCCESS'
                                  ? undefined
                                  : ColorPrimaryEnum.red
                              }
                              borderWidth={status === 'SUCCESS' ? undefined : 1}
                              padding={PaddingEnum['1x']}>
                              <Text
                                color={
                                  status === 'SUCCESS'
                                    ? ColorBaseEnum.white
                                    : ColorBaseGrayEnum.gray600
                                }
                                label={
                                  status === 'SUCCESS'
                                    ? 'Berhasil'
                                    : 'Pengecekan'
                                }
                              />
                            </Badge.Normal>
                          </Col>
                        </Row>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })
            : dataListTransaction &&
              Object.keys(dataListTransaction)?.map((data, index) => {
                const {
                  amount,
                  beneficiary_bank,
                  sender_bank,
                  beneficiary_name,
                  status,
                  created_at,
                } = dataListTransaction[data];
                return (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate(
                        'DetailTransaction',
                        dataListTransaction[data],
                      )
                    }
                    key={index}>
                    <View
                      style={{
                        marginTop: MarginEnum['2x'],
                        paddingLeft: PaddingEnum['2x'],
                        borderTopLeftRadius: BorderRadiusEnum['2x'],
                        borderBottomLeftRadius: BorderRadiusEnum['2x'],
                        backgroundColor:
                          status === 'SUCCESS'
                            ? ColorSemanticPositiveEnum.light
                            : ColorSemanticDangerEnum.light,
                        borderTopRightRadius: BorderRadiusEnum['2x'],
                        borderBottomRightRadius: BorderRadiusEnum['2x'],
                      }}>
                      <View
                        style={{
                          padding: PaddingEnum['2x'],
                          backgroundColor: ColorBaseEnum.white,
                          borderTopRightRadius: BorderRadiusEnum['2x'],
                          borderBottomRightRadius: BorderRadiusEnum['2x'],
                        }}>
                        <Row justifyContent="space-between">
                          <Col>
                            <Row>
                              <Col>
                                <Text
                                  label={
                                    sender_bank.length > 3
                                      ? sender_bank.charAt(0).toUpperCase() +
                                        sender_bank.slice(1)
                                      : sender_bank.toUpperCase()
                                  }
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="large"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <IconIonicons
                                  size={SizeEnum['5x']}
                                  name="arrow-forward"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <Text
                                  label={
                                    beneficiary_bank.length > 3
                                      ? beneficiary_bank
                                          .charAt(0)
                                          .toUpperCase() +
                                        beneficiary_bank.slice(1)
                                      : beneficiary_bank.toUpperCase()
                                  }
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="large"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                            </Row>
                            <Text
                              label={beneficiary_name.toUpperCase()}
                              color={ColorBaseGrayEnum.gray600}
                            />
                            <Row>
                              <Col>
                                <Text
                                  label={currencyFormatter(amount)}
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="small"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <IconEntypo
                                  size={SizeEnum['5x']}
                                  name="dot-single"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                              <Col>
                                <Text
                                  label={`${new Date(created_at).getDate()} ${
                                    monthNames[new Date(created_at).getMonth()]
                                  } ${new Date(created_at).getFullYear()}`}
                                  fontWeight={FontWeightSizeEnum.bold}
                                  variant="micro"
                                  color={ColorBaseGrayEnum.gray600}
                                />
                              </Col>
                            </Row>
                          </Col>
                          <Col>
                            <Badge.Normal
                              variant={
                                status === 'SUCCESS' ? 'success' : 'danger'
                              }
                              borderRadius={BorderRadiusEnum['1x']}
                              backgroundColor={
                                status === 'SUCCESS'
                                  ? undefined
                                  : ColorBaseEnum.white
                              }
                              borderColor={
                                status === 'SUCCESS'
                                  ? undefined
                                  : ColorPrimaryEnum.red
                              }
                              borderWidth={status === 'SUCCESS' ? undefined : 1}
                              padding={PaddingEnum['1x']}>
                              <Text
                                color={
                                  status === 'SUCCESS'
                                    ? ColorBaseEnum.white
                                    : ColorBaseGrayEnum.gray600
                                }
                                label={
                                  status === 'SUCCESS'
                                    ? 'Berhasil'
                                    : 'Pengecekan'
                                }
                              />
                            </Badge.Normal>
                          </Col>
                        </Row>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })}
        </View>
      </BasicLayout>
      <ModalSpinner
        message={loadingVisible.label}
        isModalVisible={loadingVisible.isShowLoading}
        sizeLoadingIcon={24}
      />
      <ModalNormal isModalVisible={isShowModalFilter} isHideButton>
        {valueFilter.map((data, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              setIsShowModalFilter(false);
              setDataFilter({
                ...dataFilter,
                name: data.name,
              });
            }}>
            <Row marginBottom={MarginEnum['4x']}>
              <Col>
                <View
                  style={{
                    width: SizeEnum['6x'],
                    height: SizeEnum['6x'],
                    backgroundColor: ColorBaseEnum.white,
                    borderColor: ColorPrimaryEnum.orange,
                    borderWidth: 2,
                    borderRadius: BorderRadiusEnum.wide,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  {data.name === dataFilter?.name && (
                    <View
                      style={{
                        width: SizeEnum['2x'],
                        height: SizeEnum['2x'],
                        backgroundColor: ColorPrimaryEnum.orange,
                        borderRadius: BorderRadiusEnum.wide,
                      }}></View>
                  )}
                </View>
              </Col>
              <Col>
                <Text marginLeft={MarginEnum['2x']} label={data.name} />
              </Col>
            </Row>
          </TouchableOpacity>
        ))}
      </ModalNormal>
    </>
  );
};

export default ListTransaction;
