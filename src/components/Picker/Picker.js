import {Picker as RNPicker} from '@react-native-picker/picker';
import {Text} from 'components/Text';
import React from 'react';
import {View} from 'react-native';
import {ColorBaseGrayEnum} from 'styles/Colors';
import {BorderRadiusEnum, MarginEnum, PaddingEnum} from 'styles/Spacer';

const Picker = (props) => {
  return (
    <>
      <Text label={props.label} marginBottom={MarginEnum['1x']} />
      <View
        style={{
          padding: PaddingEnum['0.5x'],
          borderWidth: 2,
          borderColor: ColorBaseGrayEnum.gray300,
          borderRadius: BorderRadiusEnum['2x'],
          marginBottom: MarginEnum['7x'],
        }}>
        <RNPicker
          selectedValue={props.value}
          onValueChange={(itemValue, itemIndex) =>
            props.handleChange(itemValue, itemIndex)
          }>
          {props.data.map((dataRelationship, indexRelationship) => (
            <RNPicker.Item
              key={indexRelationship}
              label={dataRelationship}
              value={dataRelationship}
            />
          ))}
        </RNPicker>
      </View>
    </>
  );
};

export default Picker;
