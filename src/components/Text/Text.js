import React from 'react';
import {Text as RNText, View} from 'react-native';
import {Skeleton} from 'components/Skeleton';

const Text = (props) => {
  if (props.isShowSkeleton) {
    return (
      <Skeleton
        color={props.colorSkeleton}
        width={props.widthSkeleton}
        height={props.heightSkeleton}
        borderRadius={props.borderRadiusSkeleton}
        marginTop={props.marginTopSkeleton}
      />
    );
  } else {
    return (
      <View style={props.textContainerStyle}>
        {props.wrapText ? (
          <RNText
            style={{
              fontSize:
                props.variant1 === 'extra-large'
                  ? 20
                  : props.variant1 === 'large'
                  ? 16
                  : props.variant1 === 'regular'
                  ? 14
                  : props.variant1 === 'small'
                  ? 13
                  : props.variant1 === 'super-small'
                  ? 12
                  : props.variant1 === 'micro'
                  ? 10
                  : 14,
              lineHeight:
                props.variant1 === 'extra-large'
                  ? 30
                  : props.variant1 === 'large'
                  ? 22
                  : props.variant1 === 'regular'
                  ? 20
                  : props.variant1 === 'small'
                  ? 18
                  : props.variant1 === 'super-small'
                  ? 16
                  : props.variant1 === 'micro'
                  ? 14
                  : 20,
              color: props.color1,
              textAlign: props.textAlign1,
              marginTop: props.marginTop1,
              marginBottom: props.marginBottom1,
              marginLeft: props.marginLeft1,
              marginRight: props.marginRight1,
              fontStyle: props.fontStyle1,
              fontWeight: props.fontWeight1,
              textAlignVertical: props.textAlignVertical1,
            }}
            textTransform={props.textTransform1}>
            {props.label1}
            <RNText
              style={{
                fontSize:
                  props.variant2 === 'extra-large'
                    ? 20
                    : props.variant2 === 'large'
                    ? 16
                    : props.variant2 === 'regular'
                    ? 14
                    : props.variant2 === 'small'
                    ? 13
                    : props.variant2 === 'super-small'
                    ? 12
                    : props.variant2 === 'micro'
                    ? 10
                    : 14,
                lineHeight:
                  props.variant2 === 'extra-large'
                    ? 30
                    : props.variant2 === 'large'
                    ? 22
                    : props.variant2 === 'regular'
                    ? 20
                    : props.variant2 === 'small'
                    ? 18
                    : props.variant2 === 'super-small'
                    ? 16
                    : props.variant2 === 'micro'
                    ? 14
                    : 20,
                color: props.color2,
                textAlign: props.textAlign2,
                marginTop: props.marginTop2,
                marginBottom: props.marginBottom2,
                marginLeft: props.marginLeft2,
                marginRight: props.marginRight2,
                fontStyle: props.fontStyle2,
                fontWeight: props.fontWeight2,
                textAlignVertical: props.textAlignVertical2,
              }}
              textTransform={props.textTransform2}>
              {props.label2}
            </RNText>
          </RNText>
        ) : (
          <RNText
            style={{
              fontSize:
                props.variant === 'extra-large'
                  ? 20
                  : props.variant === 'large'
                  ? 16
                  : props.variant === 'regular'
                  ? 14
                  : props.variant === 'small'
                  ? 13
                  : props.variant === 'super-small'
                  ? 12
                  : props.variant === 'micro'
                  ? 10
                  : 14,
              lineHeight:
                props.variant === 'extra-large'
                  ? 30
                  : props.variant === 'large'
                  ? 22
                  : props.variant === 'regular'
                  ? 20
                  : props.variant === 'small'
                  ? 18
                  : props.variant === 'super-small'
                  ? 16
                  : props.variant === 'micro'
                  ? 14
                  : 20,
              color: props.color,
              textDecorationLine: props.textDecorationLine,
              textAlign: props.textAlign,
              marginTop: props.marginTop,
              marginBottom: props.marginBottom,
              marginLeft: props.marginLeft,
              marginRight: props.marginRight,
              fontStyle: props.fontStyle,
              fontWeight: props.fontWeight,
              textAlignVertical: props.textAlignVertical,
              padding: props.padding,
              borderRadius: props.borderRadius,
              opacity: props.opacity,
            }}
            textTransform={props.textTransform}>
            {props.label}
          </RNText>
        )}
      </View>
    );
  }
};

export default Text;
