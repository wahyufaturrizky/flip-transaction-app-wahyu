import {Text} from 'components/Text';
import React from 'react';
import {Col} from 'layout/Col';
import {Row} from 'layout/Row';
import {ActivityIndicator, TouchableOpacity, View} from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  ColorBaseEnum,
  ColorBaseGrayEnum,
  ColorPrimaryEnum,
} from 'styles/Colors';
import {BorderRadiusEnum, PaddingEnum} from 'styles/Spacer';
import {FontWeightSizeEnum, MarginEnum} from '../../styles/Spacer';

export const ButtonNormal = (props) => {
  const textColorPrimary = props.isDisable
    ? ColorBaseGrayEnum.gray400
    : ColorBaseEnum.white;
  const textColorSecondary = props.isDisable
    ? ColorBaseGrayEnum.gray400
    : ColorPrimaryEnum.orange;

  if (props.variant === 'secondary') {
    return (
      <TouchableOpacity onPress={props.isDisable ? undefined : props.onPress}>
        <View
          style={{
            padding: props.padding || PaddingEnum['2.5x'],
            borderRadius: props.borderRadius || BorderRadiusEnum['1x'],
            borderWidth: 2,
            borderColor: props.isDisable
              ? ColorBaseGrayEnum.gray300
              : ColorPrimaryEnum.orange,
          }}>
          <Row justifyContent="center">
            {props.withIcon && (
              <Col>
                <View style={{marginRight: MarginEnum['2x']}}>
                  <IconMaterialIcons
                    name={props.nameIcon}
                    color={ColorPrimaryEnum.orange}
                    size={props.sizeIcon}
                  />
                </View>
              </Col>
            )}
            <Col>
              <Text
                label={props.label}
                color={textColorSecondary}
                textTransform="capitalize"
                textAlign="center"
                bold
              />
            </Col>
          </Row>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity onPress={props.isDisable ? undefined : props.onPress}>
      <View
        style={{
          padding: props.padding || PaddingEnum['2.5x'],
          borderRadius:
            props.borderTopRightRadius || props.borderBottomRightRadius
              ? null
              : BorderRadiusEnum['1x'],
          borderTopRightRadius: props.borderTopRightRadius,
          borderBottomRightRadius: props.borderBottomRightRadius,
          backgroundColor: props.isDisable
            ? ColorBaseGrayEnum.gray300
            : ColorPrimaryEnum.orange,
          marginBottom: props.marginBottom,
          marginTop: props.marginTop,
          borderWidth: 2,
          borderColor: props.isDisable
            ? ColorBaseGrayEnum.gray300
            : ColorPrimaryEnum.orange,
        }}>
        {props.isLoading !== undefined ? (
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            {props.isLoading ? (
              <ActivityIndicator color={ColorBaseEnum.white} />
            ) : (
              <Text
                label={props.label}
                color={textColorPrimary}
                textTransform="capitalize"
                textAlign="center"
                fontWeight={FontWeightSizeEnum.bold}
              />
            )}
          </View>
        ) : (
          <Text
            label={props.label}
            color={textColorPrimary}
            textTransform="capitalize"
            textAlign="center"
            fontWeight={FontWeightSizeEnum.bold}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};
