import {BasicLayout} from 'layout/BasicLayout';
import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import {ColorBaseGrayEnum} from 'styles/Colors';

const Loading = (props) => (
  <BasicLayout>
    <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
      <ActivityIndicator
        size={props.size || 'large'}
        color={props.color || ColorBaseGrayEnum.gray700}
      />
    </View>
  </BasicLayout>
);

Loading;

export default Loading;
