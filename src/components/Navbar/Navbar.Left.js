import {Button} from 'components/Button';
import {Text} from 'components/Text';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import {View} from 'react-native';
import {alertBackAction} from 'utils/alertBackAction';
import {MarginEnum, SizeEnum} from 'styles/Spacer';
import {Col} from 'layout/Col';
import {Row} from 'layout/Row';
import {
  ColorBaseGrayEnum,
  ColorBaseEnum,
  ColorPrimaryEnum,
} from 'styles/Colors';
import {FontWeightSizeEnum} from 'styles/Spacer';
import FWDColorLogo from '@assets/images/FWDColorLogo.svg';

// navigation: any because it's same as navigation props from react-native-navigation library
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const NavbarLeft = (props) => (
  <View
    style={{padding: MarginEnum['4x'], backgroundColor: ColorBaseEnum.white}}>
    <Row justifyContent="space-between">
      {props.withIcon && (
        <Col>
          <Button.Link
            onPress={
              props.onPress
                ? props.onPress
                : () =>
                    props.isExitBack
                      ? alertBackAction(props.navigation)
                      : props.navigation.goBack()
            }
            variant="navbar">
            <IconIonicons
              color={ColorBaseGrayEnum.gray600}
              name="arrow-back"
              size={24}
            />
          </Button.Link>
        </Col>
      )}
      {props.withLogo && (
        <Col>
          <FWDColorLogo />
        </Col>
      )}
      {props.withTitle && (
        <Col>
          <Text
            fontWeight={FontWeightSizeEnum.bold}
            color={ColorBaseGrayEnum.gray600}
            label={props.route.title}
          />
        </Col>
      )}
      <Col>
        <Row>
          <Col>
            <Text label="EN | " color={ColorPrimaryEnum.orange} />
          </Col>
          <Col>
            <Text color={ColorPrimaryEnum.orange} label="TH" />
          </Col>
          <Col>
            <IconIonicons
              style={{marginLeft: MarginEnum['1x']}}
              size={SizeEnum['4x']}
              name="chatbubble-ellipses-outline"
              color={ColorPrimaryEnum.orange}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  </View>
);

export default NavbarLeft;
