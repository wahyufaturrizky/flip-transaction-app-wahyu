import IconIonicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import {TextInput, View} from 'react-native';
import {ColorBaseGrayEnum} from 'styles/Colors';
import {SizeEnum} from 'styles/Spacer';

const NavbarSearch = (props) => {
  return (
    <View>
      <TextInput
        style={{
          height: 45,
          borderColor: ColorBaseGrayEnum.gray200,
          borderWidth: 1,
          backgroundColor: props.backgroundColor || ColorBaseGrayEnum.gray200,
          borderRadius: 12,
          paddingLeft: props.icon ? 40 : 15,
          color: ColorBaseGrayEnum.gray400,
          fontWeight: '500',
          marginTop: 16,
        }}
        placeholder={props.placeholder || 'Cari menu'}
        onChangeText={props.setSearchText}
        value={props.searchText || ''}
      />
      {props.icon && (
        <View style={{position: 'absolute', top: 25, marginLeft: 10}}>
          <IconIonicons size={SizeEnum['5x']} name={props.icon} />
        </View>
      )}
    </View>
  );
};

export default NavbarSearch;
