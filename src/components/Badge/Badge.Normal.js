import React from 'react';
import {View} from 'react-native';
import {
  ColorBaseEnum,
  ColorBaseGrayEnum,
  ColorSemanticDangerEnum,
  ColorSemanticInfoEnum,
  ColorSemanticWarningEnum,
  ColorSemanticPositiveEnum,
} from 'styles/Colors';
import {BorderRadiusEnum, PaddingEnum} from 'styles/Spacer';

export const BadgeNormal = (props) => {
  return (
    <View
      style={{
        padding: props.padding,
        paddingHorizontal: props.paddingHorizontal,
        paddingVertical: props.paddingVertical,
        marginRight: props.marginRight,
        alignItems: 'center',
        borderColor: props.borderColor,
        borderWidth: props.borderWidth,
        borderRadius: props.borderRadius || BorderRadiusEnum.wide,
        backgroundColor:
          props.backgroundColor ||
          (props.variant === 'info'
            ? ColorSemanticInfoEnum.lighter
            : props.variant === 'danger'
            ? ColorSemanticDangerEnum.lighter
            : props.variant === 'warning'
            ? ColorSemanticWarningEnum.lighter
            : props.variant === 'white'
            ? ColorBaseEnum.white
            : props.variant === 'success'
            ? ColorSemanticPositiveEnum.default
            : ColorBaseGrayEnum.gray200),
        color:
          props.variant === 'info'
            ? ColorSemanticInfoEnum.darker
            : props.variant === 'danger'
            ? ColorSemanticDangerEnum.darker
            : props.variant === 'warning'
            ? ColorSemanticWarningEnum.darker
            : props.variant === 'white'
            ? ColorBaseGrayEnum.gray700
            : props.variant === 'success'
            ? ColorBaseEnum.white
            : ColorBaseGrayEnum.gray400,
      }}>
      {props.children}
    </View>
  );
};
