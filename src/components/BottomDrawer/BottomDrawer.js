import React from 'react';
import {Modal, Image, View, TouchableOpacity} from 'react-native';
import {ColorBaseEnum, ColorBaseGrayEnum} from 'styles/Colors';
import {BorderRadiusEnum, FontWeightSizeEnum, PaddingEnum} from 'styles/Spacer';
import {Text} from 'components/Text';
import {Title} from 'components/Title';

const BottomDrawer = (props) => {
  return (
    <Modal
      animationType={props.animationType}
      transparent={props.transparent}
      visible={props.visible}
      onRequestClose={props.onRequestClose}>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          backgroundColor: 'rgba(0,0,0,0.6)',
        }}>
        <TouchableOpacity
          onPress={props.onRequestClose}
          style={{position: 'absolute', top: 20, left: 20}}>
          <Image
            style={{
              borderColor: '#16212D',
              borderWidth: 1,
              borderRadius: 10,
              backgroundColor: 'white',
            }}
            source={require('../../assets/images/close.png')}
          />
        </TouchableOpacity>
        <View
          style={{
            backgroundColor: ColorBaseGrayEnum.gray300,
            borderTopRightRadius: BorderRadiusEnum['4x'],
            borderTopLeftRadius: BorderRadiusEnum['4x'],
            padding: PaddingEnum['7x'],
            shadowColor: ColorBaseEnum.black,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>
          <Title
            variant={props.headerTextVariant || 'title3'}
            label={props.headerTitle}
            color={ColorBaseGrayEnum.gray600}
            textAlign={props.headerTextAlign}
            fontWeight={FontWeightSizeEnum['bold']}
          />
          <Text
            variant={props.subHeaderTextVariant || 'regular'}
            label={props.subHeaderTitle}
            color={ColorBaseGrayEnum.gray500}
            textAlign={props.subHeaderTextAlign}
            marginBottom={props.paddingBottom}
          />
          {props.children}
        </View>
      </View>
    </Modal>
  );
};

export default BottomDrawer;
