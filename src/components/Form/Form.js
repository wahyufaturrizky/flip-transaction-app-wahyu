import {Text} from 'components/Text';
import React from 'react';
import {TextInput, View} from 'react-native';
import {Col} from 'layout/Col';
import {Row} from 'layout/Row';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  BorderRadiusEnum,
  MarginEnum,
  PaddingEnum,
  SizeEnum,
} from 'styles/Spacer';
import {Group} from './Form.Group';
import {ColorBaseGrayEnum, ColorBaseEnum} from 'styles/Colors';

export const FormInput = (props) => (
  <Form.Group
    errorMessage={props.errorMessage}
    onPressInfo={props.onPressInfo}
    label={props.label}
    bold={props.labelBold}
    labelColor={props.labelColor}
    labelTextTransform={props.labelTextTransform}
    textVariant={props.labelTextVariant}
    required={props.required}>
    {!props.isPreText && !props.withIcon ? (
      <TextInput
        style={{
          backgroundColor: props.isDisabled
            ? ColorBaseGrayEnum.gray300
            : ColorBaseEnum.white,
          color: props.isDisabled
            ? ColorBaseGrayEnum.gray500
            : ColorBaseGrayEnum.gray700,
          padding: PaddingEnum['2x'],
          marginBottom: MarginEnum['1x'],
          borderRadius: BorderRadiusEnum['2x'],
          fontSize: SizeEnum['2.5x'],
          borderWidth: props.withBorder ? 2 : null,
          borderColor: props.withBorder ? ColorBaseGrayEnum.gray300 : null,
        }}
        onChangeText={(text) => props.handleChange(text)}
        onFocus={props.handleFocus}
        value={props?.value?.toString()}
        spellCheck={false}
        editable={!props.isDisabled}
        keyboardType={props.keyboardType}
        placeholder={props.placeholder}
        onSubmitEditing={props.onSubmitEditing}
        secureTextEntry={props.secureTextEntry}
      />
    ) : props.withIcon ? (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          backgroundColor: '#fff',
          borderRadius: BorderRadiusEnum['2x'],
          // Shadow style
          shadowRadius: 2,
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,
          elevation: 12,
        }}>
        <Icon
          style={{marginLeft: MarginEnum['2x']}}
          name="magnify"
          size={20}
          color="#949494"
        />
        <TextInput
          style={{
            backgroundColor: props.isDisabled
              ? ColorBaseGrayEnum.gray300
              : ColorBaseEnum.white,
            color: props.isDisabled
              ? ColorBaseGrayEnum.gray500
              : ColorBaseGrayEnum.gray700,
            padding: PaddingEnum['2x'],
            paddingRight: props.paddingRight,
            marginBottom: MarginEnum['1x'],
            borderRadius: BorderRadiusEnum['2x'],
            fontSize: SizeEnum['2.5x'],
            borderWidth: props.withBorder ? 2 : null,
            borderColor: props.withBorder ? ColorBaseGrayEnum.gray300 : null,
          }}
          onChangeText={(text) => props.handleChange(text)}
          onFocus={props.handleFocus}
          value={props?.value?.toString()}
          spellCheck={false}
          keyboardType={props.keyboardType}
          placeholder={props.placeholder}
          onSubmitEditing={props.onSubmitEditing}
          secureTextEntry={props.secureTextEntry}
        />
      </View>
    ) : (
      <View
        style={{
          backgroundColor: props.isDisabled
            ? ColorBaseGrayEnum.gray300
            : ColorBaseEnum.white,
          padding: 15,
          marginBottom: 8,
          borderRadius: 12,
        }}>
        <Row alignItems="alignItems">
          <Col size={1}>
            <Text
              label="Rp"
              color={
                props.isDisabled
                  ? ColorBaseGrayEnum.gray400
                  : ColorBaseGrayEnum.gray700
              }
              fontStyle={props.preTextFontStyle}
            />
          </Col>
          <Col size={9}>
            <TextInput
              style={{
                padding: PaddingEnum['0x'],
                marginLeft: MarginEnum['2x'],
                fontSize: SizeEnum['2.5x'],
                color: ColorBaseGrayEnum.gray700,
              }}
              onChangeText={(text) => props.handleChange(text)}
              onFocus={props.handleFocus}
              value={props?.value?.toString()}
              spellCheck={false}
              keyboardType={props.keyboardType}
              placeholder={props.placeholder}
              onSubmitEditing={props.onSubmitEditing}
            />
          </Col>
        </Row>
      </View>
    )}
    {props.exampleValue && props.exampleValueWithVariantFont ? (
      <Row>
        <Text
          marginBottom={MarginEnum['2x']}
          label={props.exampleValue}
          color={ColorBaseGrayEnum.gray500}
          variant="super-small"
          marginRight={MarginEnum['0.5x']}
        />
        <Text
          marginBottom={MarginEnum['2x']}
          label={props.exampleValueWithVariantFont}
          color={ColorBaseGrayEnum.gray500}
          variant="super-small"
          bold
        />
      </Row>
    ) : (
      <Text
        marginBottom={MarginEnum['2x']}
        label={props.exampleValue}
        color={ColorBaseGrayEnum.gray500}
        variant="super-small"
      />
    )}
  </Form.Group>
);

const Form = (props: {children: React.ReactNode}) => {
  return props.children;
};

Form.Group = Group;

export default Form;
