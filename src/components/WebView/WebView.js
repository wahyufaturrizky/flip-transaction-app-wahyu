import React from 'react';
import WebVIew from 'react-native-webview';

const WebView = (props) => {
  return <WebVIew {...props} />;
};

export default WebView;
