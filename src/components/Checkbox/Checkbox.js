import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  ColorBaseEnum,
  ColorBaseGrayEnum,
  ColorPrimaryEnum,
} from 'styles/Colors';
import {BorderRadiusEnum, SizeEnum} from 'styles/Spacer';

const Checkbox = (props) =>
  props.variant === 'radiobutton' ? (
    <TouchableOpacity
      accessibilityLabel={props.accessibilityLabel}
      onPress={() => !props.disabled && props.onPress()}>
      <View
        style={{
          width: SizeEnum['4x'],
          height: SizeEnum['4x'],
          borderRadius: BorderRadiusEnum.wide,
          borderWidth: 2,
          backgroundColor:
            props.isChecked || props.indeterminate
              ? ColorPrimaryEnum.orange
              : ColorBaseGrayEnum.gray300,
          borderColor:
            props.isChecked || props.indeterminate
              ? ColorPrimaryEnum.orange
              : ColorBaseGrayEnum.gray300,
        }}
        // style={
        //   props.disabled && {
        //     backgroundColor: ColorBaseGrayEnum.gray200,
        //     borderColor: ColorBaseGrayEnum.gray200,
        //   }
        // }
      />
    </TouchableOpacity>
  ) : (
    <TouchableOpacity
      accessibilityLabel={props.accessibilityLabel}
      onPress={() => !props.disabled && props.onPress()}>
      <View
        style={{
          width: SizeEnum['4x'],
          height: SizeEnum['4x'],
          borderRadius: BorderRadiusEnum['0.5x'],
          borderWidth: 2,
          backgroundColor:
            props.isChecked || props.indeterminate
              ? ColorPrimaryEnum.orange
              : ColorBaseGrayEnum.gray300,
          borderColor:
            props.isChecked || props.indeterminate
              ? ColorPrimaryEnum.orange
              : ColorBaseGrayEnum.gray300,
        }}
        // style={
        //   props.disabled && {
        //     backgroundColor: ColorBaseGrayEnum.gray200,
        //     borderColor: ColorBaseGrayEnum.gray200,
        //   }
        // }
      >
        {props.indeterminate ? (
          <IconMaterialIcons
            name="indeterminate-check-box"
            size="16px"
            color={ColorBaseEnum.black}
          />
        ) : (
          props.isChecked && (
            <IconMaterialIcons
              name="check"
              size={SizeEnum['3x']}
              color={
                props.disabled ? ColorBaseGrayEnum.gray400 : props.checkColor
              }
            />
          )
        )}
      </View>
    </TouchableOpacity>
  );

export default Checkbox;
