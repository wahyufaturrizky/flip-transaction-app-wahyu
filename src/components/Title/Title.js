import React from 'react';
import {Text as RNText, View} from 'react-native';

const Title = (props) => {
  return (
    <View style={props.textContainerStyle}>
      {props.wrapText ? (
        <RNText
          style={{
            fontSize:
              props.variant1 === 'title1'
                ? 36
                : props.variant1 === 'title2'
                ? 24
                : props.variant1 === 'title3'
                ? 20
                : props.variant1 === 'subtitle'
                ? 18
                : 36,
            lineHeight:
              props.variant1 === 'title1'
                ? 52
                : props.variant1 === 'title2'
                ? 36
                : props.variant1 === 'title3'
                ? 32
                : props.variant1 === 'subtitle'
                ? 26
                : 52,
            color: props.color1,
            textAlign: props.textAlign1,
            marginTop: props.marginTop1,
            marginBottom: props.marginBottom1,
            marginLeft: props.marginLeft1,
            marginRight: props.marginRight1,
            fontStyle: props.fontStyle1,
            fontWeight: props.fontWeight1,
            textAlignVertical: props.textAlignVertical1,
          }}
          textTransform={props.textTransform1}>
          {props.label1}
          <RNText
            style={{
              fontSize:
                props.variant2 === 'title1'
                  ? 36
                  : props.variant2 === 'title2'
                  ? 24
                  : props.variant2 === 'title3'
                  ? 20
                  : props.variant2 === 'subtitle'
                  ? 18
                  : 36,
              lineHeight:
                props.variant2 === 'title1'
                  ? 52
                  : props.variant2 === 'title2'
                  ? 36
                  : props.variant2 === 'title3'
                  ? 32
                  : props.variant2 === 'subtitle'
                  ? 26
                  : 52,
              color: props.color2,
              textAlign: props.textAlign2,
              marginTop: props.marginTop2,
              marginBottom: props.marginBottom2,
              marginLeft: props.marginLeft2,
              marginRight: props.marginRight2,
              fontStyle: props.fontStyle2,
              fontWeight: props.fontWeight2,
              textAlignVertical: props.textAlignVertical2,
            }}
            textTransform={props.textTransform2}>
            {props.label2}
          </RNText>
        </RNText>
      ) : (
        <RNText
          style={{
            fontSize:
              props.variant === 'title1'
                ? 36
                : props.variant === 'title2'
                ? 24
                : props.variant === 'title3'
                ? 20
                : props.variant === 'subtitle'
                ? 18
                : 36,
            lineHeight:
              props.variant === 'title1'
                ? 52
                : props.variant === 'title2'
                ? 36
                : props.variant === 'title3'
                ? 32
                : props.variant === 'subtitle'
                ? 26
                : 52,
            color: props.color,
            textAlign: props.textAlign,
            marginTop: props.marginTop,
            marginBottom: props.marginBottom,
            marginLeft: props.marginLeft,
            marginRight: props.marginRight,
            fontStyle: props.fontStyle,
            fontWeight: props.fontWeight,
            textAlignVertical: props.textAlignVertical,
          }}
          textTransform={props.textTransform}>
          {props.label}
        </RNText>
      )}
    </View>
  );
};

export default Title;
