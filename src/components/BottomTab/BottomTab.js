import {Text} from 'components/Text';
import React from 'react';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {
  ColorBaseGrayEnum,
  ColorPrimaryEnum,
  ColorBaseEnum,
} from 'styles/Colors';
import {PaddingEnum, SizeEnum, BorderRadiusEnum} from 'styles/Spacer';

const BottomTab = () => {
  return (
    <View
      style={[
        styles.shadow,
        {
          backgroundColor: ColorBaseEnum.white,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          paddingTop: PaddingEnum['1x'],
          paddingBottom: PaddingEnum['1x'],
          borderTopRightRadius: BorderRadiusEnum['3x'],
          borderTopLeftRadius: BorderRadiusEnum['3x'],
        },
      ]}>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconAntDesign
            name="home"
            size={SizeEnum['5x']}
            color={ColorBaseGrayEnum.gray500}
          />
          <Text
            label="Home"
            color={ColorBaseGrayEnum.gray500}
            variant="micro"
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconFontAwesome5
            name="hand-holding-usd"
            size={SizeEnum['5x']}
            color={ColorBaseGrayEnum.gray500}
          />
          <Text
            color={ColorBaseGrayEnum.gray500}
            label="My Claim"
            variant="micro"
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconFontAwesome5
            name="play"
            size={SizeEnum['5x']}
            color={ColorPrimaryEnum.orange}
          />
          <Text
            label="Explore"
            color={ColorPrimaryEnum.orange}
            variant="micro"
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconAntDesign
            name="filetext1"
            size={SizeEnum['5x']}
            color={ColorBaseGrayEnum.gray500}
          />
          <Text
            label="My Policy"
            color={ColorBaseGrayEnum.gray500}
            variant="micro"
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconAntDesign
            name="user"
            size={SizeEnum['5x']}
            color={ColorBaseGrayEnum.gray500}
          />
          <Text
            label="My Info"
            color={ColorBaseGrayEnum.gray500}
            variant="micro"
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
});
