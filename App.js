import {NavigationContainer} from '@react-navigation/native';
// Must Navigation V.5
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import ListTransactionScreen from 'screens/ListTransaction';
import DetailTransactionScreen from 'screens/DetailTransaction';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ListTransaction" headerMode={'none'}>
        <Stack.Screen
          name="ListTransaction"
          component={ListTransactionScreen}
        />
        <Stack.Screen
          name="DetailTransaction"
          component={DetailTransactionScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
